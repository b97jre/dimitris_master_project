===============================
lts_workflows_sm_scrnaseq
===============================

.. image:: https://readthedocs.org/projects/lts-workflows-sm-scrnaseq/badge/?version=latest
	:target: https://lts-workflows-sm-scrnaseq.readthedocs.io/en/latest/?badge=latest
	:alt: Documentation Status

.. image:: https://anaconda.org/scilifelab-lts/lts-workflows-sm-scrnaseq/badges/version.svg
	   :target: https://anaconda.org/scilifelab-lts/lts-workflows-sm-scrnaseq

.. image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
	   :target: http://www.gnu.org/licenses/gpl-3.0


lts_workflows_sm_scrnaseq is a single-cell RNA sequencing workflow in
snakemake. Briefly, the following steps are run:

1. align reads with STAR to a genome or transcriptome reference
2. gene/transcript quantification with RSEM and/or rpkmforgenes
3. basic qc with RSeQC and MultiQC

For more information see `the documentation on readthedocs
<https://readthedocs.org/projects/lts-workflows-sm-scrnaseq>`_.


Quickstart
----------

Install and run via conda
+++++++++++++++++++++++++

Make sure your channel order complies with `the instructions provided
by bioconda <https://bioconda.github.io/#set-up-channels>`_. In short,
your *~/.condarc* file should have the following channel configuration:

.. code-block:: yaml

   channels:
     - bioconda
     - conda-forge
     - defaults

Note that the workflow depends on some python2 packages that by
default are accessed from a conda environment named `py2.7`. If you
are not using the ``--use-conda`` flag, this environment needs to be
created with the required dependencies.

.. code-block:: console
   $ conda create -n py2.7 python=2.7 rpkmforgenes=1.0.1 rseqc=2.6.4

The preferred way to install is to first create a new environment,
activate it and install the package:

.. code-block:: console

   $ conda create -n lts-workflows-sm-scrnaseq python=3.6
   $ source activate lts-workflows-sm-scrnaseq
   $ conda install -c scilifelab-lts lts-workflows-sm-scrnaseq

Example commands:

.. code-block:: console

   $ lts_workflows_sm_scrnaseq
   $ lts_workflows_sm_scrnaseq -l
   $ lts_workflows_sm_scrnaseq all -d /path/to/workdir --configfile config.yaml
   $ lts_workflows_sm_scrnaseq --use-conda all -d /path/to/workdir --configfile config.yaml

The package ships a workflow test. Run

.. code-block:: console

   $ pytest -v -rs -s --pyargs lts_workflows_sm_scrnaseq

to see it in action.

Docker image
++++++++++++

.. code-block:: console

   $ docker pull scilifelablts/lts-workflows-sm-scrnaseq
   $ docker run scilifelablts/lts-workflows-sm-scrnaseq
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts-workflows-sm-scrnaseq -l
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts-workflows-sm-scrnaseq all

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
