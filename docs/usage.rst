.. _usage:

Usage
=====

This section provides some simple usage examples. For more information
what the workflow does, see :ref:`workflow`.

.. _running_wrapper:

Running the wrapper script
--------------------------

:mod:`lts_workflows_sm_scrnaseq` comes with a wrapper script that
calls snakemake and provides additional help messages. Running the
wrapper without any arguments will generate a help message. If any
arguments are provided they are passed on to snakemake.

.. code-block:: console

   $ lts_workflows_sm_scrnaseq
   $ lts_workflows_sm_scrnaseq -l
   $ lts_workflows_sm_scrnaseq all
   $ lts_workflows_sm_scrnaseq --use-conda all
   $ lts_workflows_sm_scrnaseq -s /path/to/Snakefile --use-conda all
		
If no Snakefile is provided, the wrapper script will automatically
load the Snakefile from the package root directory (see
`example_snakefile`_). Note that you will have to pass a configuration
file with the `--configfile` parameter.

In the case you need to add custom rules, or want to hardcode
parameters such as the working directory and configuration file, you
can of course copy and edit the provided Snakefile.

.. _running_snakemake:

Running snakemake
------------------

You can of course bypass the provided wrapper script and run snakemake
directly on your own Snakefile. If so, the intended usage is to
include the main workflow file in a Snakefile. See the examples in the
test directory. 

.. code-block:: console
		
   $ snakemake -s Snakefile -d /path/to/workdir --configfile config.yaml all


Running docker/singularity containers
--------------------------------------

.. note::

   Singularity is still WIP.

:mod:`lts_workflows_sm_scrnaseq` is also shipped with all the
dependencies packaged in a docker image. This eliminates some of the
installation issues, at the cost of having to download a large image
file (>5GB). In any case, the entry point of the image points to the
:mod:`lts_workflows_sm_scrnaseq` wrapper script.

.. code-block:: console

   $ docker pull scilifelablts/lts_workflows_sm_scrnaseq
   $ docker run scilifelablts/lts_workflows_sm_scrnaseq
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts_workflows_sm_scrnaseq -l
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts_workflows_sm_scrnaseq all

.. _example_snakefile:

Example Snakefile
-------------------

The provided minimum Snakefile looks as follows:

.. code-block:: python
		
   # -*- snakemake -*-
   from lts_workflows_sm_scrnaseq import WORKFLOW

   include: WORKFLOW


