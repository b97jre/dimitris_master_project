.. _troubleshooting:

Troubleshooting
================

Installation of rsem hangs when using --use-conda
-------------------------------------------------

This is a known conda issue (`#5536
<https://github.com/conda/conda/issues/5536>`_). One suggested
solution is to remove the r channel from defaults in `~/.condarc`:

.. code-block:: yaml

   default_channels:
   - https://repo.continuum.io/pkgs/free
   - https://repo.continuum.io/pkgs/pro

Also make sure your package order follows the ordering from bioconda:

.. code-block:: yaml

   channels:
     - bioconda
     - conda-forge
     - defaults


The workflow cannot find RSeQC or rpkmforgenes
-----------------------------------------------

Make sure you have created a conda environment for python2 packages
whose name matches that of the configuration setting
`config['conda']['python2']` (default is ``py2.7``).


STAR align cannot find the input files even though they are present
-------------------------------------------------------------------

The input fastq file names depend on the configuration setting
`config['settings']['runfmt']` as well as
`config['ngs.settings']['read1_label']` (`read2_label` for read 2) and
`config['ngs.settings']['fastq_suffix']`. Make sure that read labels
and fastq suffix are correctly configured.
