.. _installation:

.. highlight:: shell

Installation
============

Although installing :mod:`lts_workflows_sm_scrnaseq` itself should be
easy enough, installing the workflow dependencies requires that you
perform some manual steps. The number of steps depend on the
installation route you take. For instance, the conda installation will
install all python3 dependencies on the fly.


.. hint::

   Whatever installation route you choose, it is recommended to create
   `conda environments <https://conda.io/docs/>`_ for python2 and
   python3 packages. The workflow will be executed from the python3
   environment and by default is configured to load a python2
   environment named `py2.7`. You can modify the python2 environment
   name in the configuration setting `config['conda']['python2']`.


Stable release from conda
-------------------------

.. note::

   Make sure your channel order complies with `the instructions
   provided by bioconda
   <https://bioconda.github.io/#set-up-channels>`_. In short, your
   *~/.condarc* file should have the following channel configuration:

   .. code-block:: yaml

      channels:
	- bioconda
	- conda-forge
	- defaults

The workflow depends on a couple of python2 packages that cannot be
added as requirement to conda. Therefore, these packages must be
installed manually.

.. code-block:: console

   $ conda create -n py2.7 python=2.7 rpkmforgenes=1.0.1 rseqc=2.6.4

To install :mod:`lts_workflows_sm_scrnaseq`, create a new environment,
activate it and install the package:

.. code-block:: console

   $ conda create -n lts-workflows-sm-scrnaseq python=3.6
   $ source activate lts-workflows-sm-scrnaseq
   $ conda install -c scilifelab-lts lts-workflows-sm-scrnaseq

This is the preferred method to install
:mod:`lts_workflows_sm_scrnaseq`, as it will always install the most
recent stable release.

You can also create a new environment with
:mod:`lts_workflows_sm_scrnaseq` as a dependency:

.. code-block:: console

   $ conda create -n lts-workflows-sm-scrnaseq -c scilifelab-lts lts-workflows-sm-scrnaseq python=3.6

From sources
------------

The sources for lts_workflows_sm_scrnaseq can be downloaded from the
`Bitbucket repo`_.

.. code-block:: console

    $ git clone git@bitbucket.org:scilifelab-lts/lts-workflows-sm-scrnaseq.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

   $ cd lts-workflows-sm-scrnaseq
   $ python setup.py install

You can also install in development mode with:

.. code-block:: console

    $ python setup.py develop

See the section on :ref:`development` for more information.

You can setup the python 2 packages as in the previous section, or by using the environment file `lts_workflows_sm_scrnaseq/environment-27.yaml`:

.. code-block:: console

   $ conda env create -n py2.7 -f lts_workflows_sm_scrnaseq/environment-27.yaml


Tests
------

If :mod:`lts_workflows_sm_scrnaseq` has been installed as a module, run

.. code-block:: console

   $ pytest -v -rs -s --pyargs lts_workflows_sm_scrnaseq

In order to load the pytest options provided by the module, the full
path to the test suite needs to be given:

.. code-block:: console

   $ pytest -v -rs -s /path/to/lts_workflows_sm_scrnaseq/tests

See :ref:`test_based_development` for more information.

.. _Bitbucket repo: https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq
