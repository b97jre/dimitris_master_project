============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at
https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the Bitbucket issues for bugs. Anything tagged with "bug"
and "help wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the Bitbucket issues for features. Anything tagged with
"enhancement" and "help wanted" is open to whoever wants to implement
it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

lts_workflows_sm_scrnaseq could always use more documentation, whether
as part of the official lts_workflows_sm_scrnaseq docs, in docstrings,
or even on the web in blog posts, articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at
https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq/issues.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

.. _contributing_get_started:
  
Get Started!
------------

Ready to contribute? Here's how to set up `lts_workflows_sm_scrnaseq`
for local development.

1. Fork the `lts_workflows_sm_scrnaseq` repo on Bitbucket.
2. Clone your fork locally::

     .. code-block:: console

	$ git clone git@bitbucket.org:your_name_here/lts-workflows-sm-scrnaseq.git

3. Follow the instructions in :ref:`development` to install a conda
   development environment and the package.
   
4. Create a branch for local development::

     .. code-block:: console

	$ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

5. When you're done making changes, check that your changes pass
   flake8 and the tests:

   .. code-block:: console

      $ make lint
      $ pytest -v -s -rs

6. Commit your changes and push your branch to Bitbucket:

   .. code-block:: console

      $ git add .
      $ git commit -m "Your detailed description of your changes."
      $ git push origin name-of-your-bugfix-or-feature

7. Submit a pull request through the Bitbucket website.

Pull Request Guidelines
-----------------------

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests (optional).
2. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature HISTORY.rst.
3. Check
   https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq/addon/pipelines/home
   and make sure that the CI tests pass.
