# -*- python -*-
# -*- coding: utf-8 -*-
import os
import sys
import argparse
import subprocess as sp
from snakemake.logging import logger
try:
    from lts_workflows.utils import RSTHelpFormatter as HelpFormatter
except:
    from argparse import HelpFormatter

import lts_workflows_sm_scrnaseq
SNAKEFILE = os.path.join(
    os.path.dirname(lts_workflows_sm_scrnaseq.__file__),
    "Snakefile")


def lts_workflows_sm_scrnaseq_wrapper():
    """lts_workflows_sm_scrnaseq
----------------------------------------------

Wrapper for running lts_workflows_sm_scrnaseq workflow. Any argument
will be passed to snakemake. Consequently, this means you *must*
supply a workflow target to run the workflow. By default, the wrapper
will use a generic Snakefile shipped with the package. Note that in
this case you must supply a configuration file via the --configfile
option.

Examples
~~~~~~~~

.. code-block:: bash

   $ lts_workflows_sm_scrnaseq -l
   $ lts_workflows_sm_scrnaseq all --configfile config.yaml -d /path/to/workdir

Docker usage
-------------

If the docker image is used to run the workflow, this wrapper serves
as the entry point. The image uses gosu to set the user id of the main
process, which defaults to user id 9001. In order to run as the local
user, the environment variable LOCAL_USER_ID must be passed to the
docker run process (recommended).

Examples
~~~~~~~~

.. code-block:: bash

   $ docker run scilifelablts/lts_workflows_sm_scrnaseq
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts_workflows_sm_scrnaseq all --configfile config.yaml
   $ docker run -e LOCAL_USER_ID=1000 -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts_workflows_sm_scrnaseq all --configfile config.yaml
   $ docker run -e LOCAL_USER_ID=1000 -v /path/to/workdir:/workspace -w /workspace --entrypoint "/bin/bash" scilifelablts/lts_workflows_sm_scrnaseq

All commands are handled by the lts_workflows_sm_scrnaseq wrapper, but
you can also explicitly call snakemake:

.. code-block:: bash

   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts_workflows_sm_scrnaseq snakemake all --configfile config.yaml


Snakefile contents
-------------------

The wrapper runs a package Snakefile with the following minimum content:

.. code-block:: python

   from lts_workflows_sm_scrnaseq import WORKFLOW
   include: WORKFLOW

If need be, extend this file with custom rules and directives and run
it with the wrapper or as usual with regular Snakemake.

    """
    if int(sys.version[0]) != 3:
        logger.error("python version 3 required to run")
        sys.exit(1)

    parser = argparse.ArgumentParser(
        description="Workflow wrapper for lts_workflows_sm_scrnaseq",
        epilog=lts_workflows_sm_scrnaseq_wrapper.__doc__,
        formatter_class=HelpFormatter,
        add_help=False)
    parser.add_argument('optargs', nargs="*",
                        help="options and arguments passed to snakemake")

    args, unknown = parser.parse_known_args()

    if len(args.optargs + unknown) == 0:
        print("\n\n")
        parser.print_help()
        print("\n\n")
    else:
        snakefile_arg = bool(set(unknown + args.optargs).intersection(['-s', '--snakefile'])) or os.path.exists("Snakefile")
        configfile_arg = set(unknown + args.optargs).intersection(['--configfile'])
        if not snakefile_arg:
            logger.warning("No Snakefile found! Using generic lts_workflows_sm_scrnaseq Snakefile: {}".format(SNAKEFILE))
            if not configfile_arg:
                logger.error("No config file passed! Make sure to supply a configuration file with --configfile parameter")
                sys.exit(1)
            unknown += ['-s', SNAKEFILE]
        cmd = ['snakemake'] + unknown + args.optargs
        sp.run(cmd)
