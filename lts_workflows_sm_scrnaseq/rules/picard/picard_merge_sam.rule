# -*- snakemake -*-
include: "picard.settings"

def _merge_suffix(aligner):
    """Return the merge *input* suffix for genome alignments"""
    align_config = config['' + aligner]
    unique = "" if config['workflow']['use_multimapped'] else "_unique"
    if aligner == "star":
        return ".Aligned.out{unique}.bam".format(unique=unique)


def _merge_tx_suffix(aligner):
    """Return the merge *input* suffix for transcriptome alignments"""
    align_config = config['' + aligner]
    tx_string = "" if config['rsem']['index_is_transcriptome'] else ".toTranscriptome"
    if aligner == "star":
        return ".Aligned{tx}.out.bam".format(tx=tx_string)


def _picard_merge_input_fn(wildcards):
    """Function to find input files for picard merge.

    Utilise information in config['settings']['sampleinfo'],
    config['settings']['runfmt'] and config['settings']['samplefmt'].
    'runfmt' and 'samplefmt' are python miniformat strings. The
    formatted runfmt string is used to generate names of input bam
    files. The current wildcard prefix corresponds to a formatted
    samplefmt string, all of which are computed and compared to the
    former.
    """
    if wildcards.label == "merge":
        logger.debug("_picard_merge_input_fn: saw 'merge' label; merging genome alignments")
        suffix = _merge_suffix(config['ngs.settings']['aligner'])
    elif wildcards.label == "merge.tx":
        logger.debug("_picard_merge_input_fn: saw 'merge.tx' label; merging transcriptome alignments")
        suffix = _merge_tx_suffix(config['ngs.settings']['aligner'])
    else:
        raise Exception("malformatted merge label {}".format(wildcards.label))
    runfmt = config['settings']['runfmt']
    samplefmt = config['settings']['samplefmt']
    samplepfx = {k:k for k in set(samplefmt.format(**s) for s in config['_sampleinfo'])}
    try:
        current_sample = samplepfx[wildcards.prefix]
    except KeyError:
        raise Exception("no such sample prefix {}; sampleinfo file malformatted?".format(wildcards.prefix))
    inputset = list(set(runfmt.format(**x) + suffix for x in config['_sampleinfo'] if samplefmt.format(**x) == current_sample))
    return inputset


config_default = {'picard' :{'merge_sam' : _picard_config_rule_default.copy()}}
config_default['picard']['merge_sam'].update(
    {
        #'inputfun' : _picard_merge_sam_input_fn,
        'options' : "CREATE_INDEX=true",
        'runtime' : '24:00:00',
        'source_suffix' : ".bam",
    })


update_config(config_default, config)
config = config_default


rule picard_merge_sam:
    """Picard: merge sam files.

    NB: always outputs bam files!

    Currently the merge labels are hardcoded, where

      merge: genome alignments
      merge.tx: transcript alignments
    """
    params: cmd = config['picard']['cmd'] + "MergeSamFiles",
            options = " ".join([config['picard']['options'],
                                config['picard']['merge_sam']['options']]),
            runtime = config['picard']['merge_sam']['runtime']
    wildcard_constraints:
        prefix = ".+",
        label = "merge(.tx)?"
    input: _picard_merge_input_fn
    output: merge="{prefix}.{label}.bam"
    threads: config['picard']['merge_sam']['threads']
    run:
      if (len(input) > 1):
          inputstr = " ".join(["INPUT={}".format(x) for x in input])
          shell("{cmd} {ips} OUTPUT={out} {opt}".format(cmd=params.cmd, ips=inputstr, out=output.merge, opt=params.options))
      else:
          if os.path.exists(output.merge):
              os.unlink(output.merge)
          shutil.copy(input[0], output.merge)



