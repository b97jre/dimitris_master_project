#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_lts_workflows_sm_scrnaseq
----------------------------------

Tests for `lts_workflows_sm_scrnaseq` module.
"""
import subprocess as sp
import itertools
import pytest
# from lts_workflows.pytest.plugin import conda_environments
import logging


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@pytest.mark.missing_pytest_ngsfixtures
@pytest.mark.parametrize(
    "runenv",
    itertools.product(
        sorted(pytest.config.option.ngs_command),
        pytest.config.option.sequencing_modes,
        pytest.config.option.ngs_unit,),
    ids=lambda x: "-".join(y for y in x),
    indirect=["runenv"])
def test_workflow(runenv):
    Snakefile, options, container, kwargs, results = runenv
    res = []
    from pytest_ngsfixtures.wm import snakemake
    if container is not None:
        container.start()
    for r in snakemake.run(Snakefile,
                           options=options,
                           container=container,
                           iterable=True,
                           save=True,
                           stderr=sp.STDOUT,
                           **kwargs):
        print(r)
        res.append(r)
    assert results in "".join(res)
