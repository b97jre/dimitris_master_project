---
title: "SC QC analysis "
author: "Dimitrios"
output: html_document

params:
  workingDir: /Users/johanreimegard/git/dimitris_master_project
  RscriptsDir: R_Analysis/Rscripts
  storageDir: /Users/johanreimegard/storage/dimitris_master_project
  countsTable: countData/feature_counts.table.tsv
  metaDataTable: information/coldata.csv
  metaDataTableQC: information/sampleData.QC.tsv
  samplesRemovedAfterQC : information/samplesRemoved.QC.tsv
  countsTableQC: countData/feature_counts.table.QC.tsv
  rpkmTableQC: countData/RPKMs.table.QC.tsv
  geneInfoQC: information/geneInfo.QC.tsv
  ERCC.countTable.QC: information/ERCC_feature_counts.table.QC.tsv
  
  
---



```{r loading the data and making sure the matrixces have the same order , include = FALSE}

source(paste(params$workingDir, params$RscriptsDir,"functions.R", sep = "/"))
source(paste(params$workingDir, params$RscriptsDir,"pca_plotting_v2.R", sep = "/"))
source(paste(params$workingDir, params$RscriptsDir,"vioplot.list.R", sep = "/"))
#library(vioplot)
#library(plotrix)
#install.packages("reshape")
library(reshape)
library(ggplot2)
library(RColorBrewer)
library(gplots)
```

# QC analysis of the seq data


## Very few ERCC are found in the data. 

Especially for bulk and FACS, 

Also a difference between the pull down protocoll and the smartseq 2 protocoll

##  Variable genes analysis using ERCC is a bit inconclusive. 

The same number of variable genes above noise level is the same for pulldown all three time points as when only comparing one time point. The results differs a lot between the time points and depending on the  quantile fit parameter. 

##Conclusion

Using ERCC for normalising the data is not a good way to do it because it differs a lot between the two protocolls. 







## Loading the sample data available from the seq names and put together. 

```{r loading sample data meta data, include= TRUE}

sampleNames = read.table(file=paste(params$workingDir,"R_Analysis/sampleName.tsv", sep = "/"),
                         sep = "\t", header = TRUE)

rownames(sampleNames) <- sampleNames$fileName

sampleNames$joint <- as.factor(paste(sampleNames$info, sampleNames$time, sampleNames$type, sep="_"))

sampleNames$ID <- as.factor(paste(sampleNames$info, sampleNames$time, sampleNames$type , sampleNames$plate, sep="_"))

summary(sampleNames)


```


```{r loading count data, include=FALSE}

exon_init <- read.table(file=paste(params$storageDir,params$countsTable, sep = "/")
                        ,header=T,sep="\t")






# merge the two lanes with the same data



#colnames(exon_init) <- substr(colnames(exon_init), 1, regexpr('\\.', colnames(exon_init)) - 1)

# Change the structure to get only the counts for exons
exon = exon_init[1:58302,]
rownames(exon)<-exon$Geneid
exon <- exon[order(row.names(exon)),]
geneInfo = exon[,1:6]
exon <- exon[,-1:-6]


exon_ecc <- exon_init[58303:nrow(exon_init),]
rownames(exon_ecc)<-exon_ecc$Geneid
exon_ecc <- exon_ecc[order(row.names(exon_ecc)),]
eccInfo = exon_ecc[,1:6]
exon_ecc <- exon_ecc[,-1:-6]





```

```{r merging the data from the lanes}

# Comb_48h_dil_C1 only found in one 


L7exon = exon[,as.character(sampleNames$fileName[sampleNames$lane == "L007"  &
                                                   sampleNames$ID != "Comb_48h_dil_C1"])]
L8exon = exon[,as.character(sampleNames$fileName[sampleNames$lane == "L008"])]


exonDouble = L7exon + L8exon
exonDouble$"Comb.48h.100.dil.C1_S398_L007_R1_001.Aligned.out_unique.bam" = exon[,sampleNames$fileName[sampleNames$ID == "Comb_48h_dil_C1"]]




ERCC_L7exon = exon_ecc[,as.character(sampleNames$fileName[sampleNames$lane == "L007"  &
                                                   sampleNames$ID != "Comb_48h_dil_C1"])]
ERCC_L8exon = exon_ecc[,as.character(sampleNames$fileName[sampleNames$lane == "L008"])]
ERCC_exonDouble = ERCC_L7exon + ERCC_L8exon
ERCC_exonDouble$"Comb.48h.100.dil.C1_S398_L007_R1_001.Aligned.out_unique.bam" = exon_ecc[,sampleNames$fileName[sampleNames$ID == "Comb_48h_dil_C1"]]



sampleNames2 = sampleNames[colnames(exonDouble),  ]
sampleNames = sampleNames2


colnames(exonDouble) = sampleNames$ID
colnames(ERCC_exonDouble) = sampleNames$ID
rownames(sampleNames) =sampleNames$ID



sampleNames2 = sampleNames[ ,c("info","time","type","plate","count","joint") ]
exon = exonDouble
exon_ecc = ERCC_exonDouble



```


```{r extract data from exon data and add to samplesInfo, include=FALSE}
# make sure that they are in the same order
sampleNames = sampleNames[colnames(exon),]

# Remove columns with less than 10 000 reads for exons

sampleNames$countSumsGene =  colSums(exon)




# remove samples with less than 10 000 counts
sampleNames = sampleNames[sampleNames$countSumsGene >= 10000,]
exon = exon[,rownames(sampleNames)]
exon_ecc = exon_ecc[,rownames(sampleNames)]


# Change the structure to get only the counts for exons_ecc

sampleNames$countSums_ecc <- colSums(exon_ecc)
sampleNames$countSumsGene =  colSums(exon)

sampleNames$countSums = sampleNames$countSumsGene + sampleNames$countSums_ecc

summary(sampleNames)

## Order the matrix samples and the sampleName matrix in the same order
```




```{r First round of normalisation using RPM and RPKM, include = FALSE}
# Calculate RPKM values for exons
RPMs <- t(t(exon)/sampleNames$countSums) * 1000000
RPKMs = RPMs / geneInfo$Length * 1000

# Calculate RPKM values for spikes
RPMs_ecc <- t(t(exon_ecc)/sampleNames$countSums) *1000000
#colSums(RPMs_ecc)
RPKMs_ecc <- RPMs_ecc/eccInfo$Length * 1000
RPKMs_df_ecc <- data.frame(RPKMs_ecc)

# Calculate RPKM values for introns
#RPMs_intron <- t(t(intron)/countSums_intron) * 1000000
#RPKMs_intron = RPMs_intron / lengths_intron * 1000


#RPKMs_final <- RPKMs - RPKMs_intron
#RPKMs_final2 <- RPKMs_intron - RPKMs
geneInfo$rpkm_mean = rowMeans(RPKMs)
rpkm_ecc_mean = rowMeans(RPKMs_ecc)
#rpkm_mean[order(-rpkm_mean)]

# make metadata annotation
# bulks 
# Plate 1_H12, Plate1_G12, Plate2_H12, Plate2_G12
```



```{r defining colours and other stuff, include=TRUE}

sampleNames$sumRPKMs = colSums(RPKMs)
sampleNames$sumRPKMs_ecc = colSums(RPKMs_ecc)
sampleNames$fracSpikes =sampleNames$sumRPKMs_ecc/(sampleNames$sumRPKMs_ecc+sampleNames$sumRPKMs)

#hist(sampleNames$fracSpikes,n=100, xlim=c(0.0,min(c(max(sampleNames$fracSpikes)+0.1, 0.6))), ylim=c(0, 80))
 
sampleNames$nDet.ercc <- apply(RPKMs_ecc,2,function(x) sum(x>1))


ggplot(sampleNames,aes(nDet.ercc)) + geom_density(aes(color = time)) + 
  facet_grid(type~info, scales = "free_y" , margins = TRUE) 
# 15 spikes with no detection, still average per cell is around 15 detected spikes.

#ggplot(sampleNames, aes(y = sumRPKMs, x = sumRPKMs_ecc,color = joint, shape = factor(info))) + geom_point()
#legend("topright",col=coldef,pch=c(16,17,16,17,16,17,16,17),legend=names(coldef))



```


```{ comparing length of genes and expression}
# OBS! some samples with no spikes detected!! Most have very low levels of spike-ins
# check number of detected spikes per sample.
geneInfo$size = as.character("small")


geneInfo$size[geneInfo$Length>1000] = "medium"
geneInfo$size[geneInfo$Length>5000] = "large"
class(RPMs)


RPMsDF  = data.frame(RPMs)
RPMsDF$geneName = rownames(RPMsDF)
RPMsDF = melt(RPMsDF, id=c("geneName"))
RPMsDF2 = RPMsDF[RPMsDF$value > 1 & RPMsDF$value < 100 ,]

RPMsDF2 = merge(RPMsDF2, sampleNames, by.x = "variable", by.y = "fileName")
RPMsDF2 = merge(RPMsDF2, geneInfo, by.x = "geneName", by.y =  "Geneid")
ggplot(RPMsDF2, aes(y = value,x = as.factor(joint),colour = info,linetype = time, fill = type)) + geom_violin(draw_quantiles = c(0.25, 0.5, 0.75)) + facet_grid(size~. )

```



```{r filtering again for spike in deviance}
exon <- as.matrix(exon)
exon_ecc <- as.matrix(exon_ecc)

# clearly 6 cells that deviates, remove those
# REMOVED Q$unique > 60 &


samples_keep = as.character(sampleNames$ID[sampleNames$fracSpikes < 0.2])




RPKMs <- RPKMs[,samples_keep]
RPKMs_ecc <- RPKMs_ecc[,samples_keep]
exon <- exon[,samples_keep]
exon_ecc <- exon_ecc[,samples_keep]
sampleNames = sampleNames[samples_keep, ]


```




### Plot the biologically variable genes


Not so many different for the different time points compared to the single time points. Depending on how many ERCC that is used (quant.fit). 

Not sure if using ERCC is the best way of normalizing the data.






#### Variable genes for all single cell all timepoints for the pulldown protocoll.

```{r Creating subsets}
# Variable genes based on ERCC

pulldown = which(sampleNames$info == "Comb" & sampleNames$type == "sc" )
pulldownbulk = which(sampleNames$info == "Comb" & sampleNames$type != "sc" )
pulldown0 = which(sampleNames$info == "Comb" & sampleNames$type == "sc" & sampleNames$time == "0h")
pulldown24 = which(sampleNames$info == "Comb" & sampleNames$type == "sc" & sampleNames$time == "24h")
pulldown48 = which(sampleNames$info == "Comb" & sampleNames$type == "sc" & sampleNames$time == "48h")
smartseq2 = which(sampleNames$info == "Ref" & sampleNames$type == "sc" )
smartseq2bulk = which(sampleNames$info == "Ref" & sampleNames$type != "sc" )
```


```{r plot the variable genes}
# Variable genes based on ERCC
v1 <- cv2.var.genes(RPKMs[,pulldown],RPKMs_ecc[,pulldown],plot = T,quant.fit=0.7)
```


#### Variable genes for all single cell 0h for the pulldown protocoll.

```{r plot the pulldown 0h variable genes}
v3 <- cv2.var.genes(RPKMs[,pulldown0],RPKMs_ecc[,pulldown0],plot = T,quant.fit=0.7)
#v5 <- cv2.var.genes(RPKMs[,pulldown0],RPKMs_ecc[,pulldown0],plot = T,quant.fit=0.6)
```
#### Variable genes for all single cell 24h for the pulldown protocoll.

```{r plot the pulldown 24h variable genes}

v3 <- cv2.var.genes(RPKMs[,pulldown24],RPKMs_ecc[,pulldown24],plot = T,quant.fit=0.7)
#v5 <- cv2.var.genes(RPKMs[,pulldown0],RPKMs_ecc[,pulldown0],plot = T,quant.fit=0.6)

```

#### Variable genes for all single cell 48h for the pulldown protocoll.

```{r plot the pulldown 48h variable genes}
#v3 <- cv2.var.genes(RPKMs[,pulldown48],RPKMs_ecc[,pulldown48],plot = T,quant.fit=0.8)
v5 <- cv2.var.genes(RPKMs[,pulldown48],RPKMs_ecc[,pulldown48],plot = T,quant.fit=0.7)

```


#### Variable genes for all single cell 0h for the Smartseq2 protocoll.

```{r plot the smartSeq2  0h variable genes}

v2 <- cv2.var.genes(R = RPKMs[,smartseq2],ERCC = RPKMs_ecc[,smartseq2],plot = T,quant.fit=0.7)
```

```{r save the files for further analysis}


write.table(x = exon,
            file=paste(params$storageDir,params$countsTableQC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = TRUE)


write.table(x = exon_ecc,
            file=paste(params$storageDir,params$ERCC.countTable.QC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = TRUE)


write.table(x = RPKMs,
            file=paste(params$storageDir,params$rpkmTableQC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = TRUE)

write.table(x = sampleNames,
            file=paste(params$storageDir,params$metaDataTableQC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = FALSE)

write.table(x = geneInfo,
            file=paste(params$storageDir,params$geneInfoQC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = FALSE)



```


```{r check which samples that was removed.}
sampleNamesbeforeQC = read.table(file=paste(params$workingDir,"R_Analysis/sampleName.tsv", sep = "/"),
                         sep = "\t", header = TRUE)



sampleNamesAfterQC = read.table(file=paste(params$storageDir,params$metaDataTableQC, sep = "/"),
                         sep = "\t", header = TRUE)

sampleNamesbeforeQC$joint = paste(sampleNamesbeforeQC$info,sampleNamesbeforeQC$time, sampleNamesbeforeQC$plate, sep = "_")
BQC = unique(sampleNamesbeforeQC$joint)

sampleNamesAfterQC$joint = paste(sampleNamesAfterQC$info,sampleNamesAfterQC$time, sampleNamesAfterQC$plate, sep = "_")
AQC = unique(sampleNamesAfterQC$joint)



samplesRemoved = data.frame(sampleNames = setdiff(BQC,AQC))

write.table(x = samplesRemoved,
            file=paste(params$storageDir,params$samplesRemovedAfterQC, sep = "/"),
            col.names = TRUE,
            sep="\t",
            quote = FALSE,
            row.names = FALSE)



```
