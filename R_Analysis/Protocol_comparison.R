## NEED TO ADD THE SCRIPTS CREATED BY ASA
setwd("/Users/dbampalikis/Dimitris Personal/0 - Bioinformatics/15 - Thesis/Tools/countTable")

source("../analysis/bin/Rscripts/functions.R")
source("../analysis/bin/Rscripts/pca_plotting_v2.R")
source("../analysis/bin/Rscripts/vioplot.list.R")
library(vioplot)
library(plotrix)
#install.packages("reshape")
library(reshape)
library(ggplot2)

exon_init <- read.table("exon_count.featureCount.table.tsv",header=T,sep="\t")
colnames(exon_init) <- substr(colnames(exon_init), 1, regexpr('\\.', colnames(exon_init)) - 1)

# Change the structure to get only the counts for exons
exon = exon_init[1:58302,]
exon_gene_names <- exon[,1]
rownames(exon)<-exon[,1]
exon <- exon[,-1:-5]
exon <- exon[order(row.names(exon)),]
lengths <- exon[,1] 
names(lengths) <- rownames(exon)
exon <- exon[,-1]
head(lengths)

# Remove columns with less than 10 000 reads for exons
countSums <- colSums(exon)
lowCount = which(countSums < 10000)
exon = exon[,-lowCount]
countSums_exon <- colSums(exon)

# Change the structure to get only the counts for exons_ecc
exon_ecc <- exon_init[58303:nrow(exon_init),]
exon_ecc_gene_names <- exon_ecc[,1]
rownames(exon_ecc)<-exon_ecc[,1]
exon_ecc <- exon_ecc[,-1:-5]
lengths_ecc <- exon_ecc[,1]
exon_ecc <- exon_ecc[,-1]
exon_ecc <- exon_ecc[order(row.names(exon_ecc)),]

exon_ecc = exon_ecc[,-lowCount]
countSums_ecc <- colSums(exon_ecc)

countSums = countSums_exon + countSums_ecc

# Change the structure to get only the counts for introns
intron_init <- read.table("gene_count.featureCount.table.tsv",header=T,sep="\t")
colnames(intron_init) <- substr(colnames(intron_init), 1, regexpr('\\.', colnames(intron_init)) - 1)
intron = intron_init
intron_gene_names <- intron[,1]
rownames(intron)<-intron[,1]
intron <- intron[,-1:-5]
intron <- intron[order(row.names(intron)),]
lengths_intron <- intron[,1] 
names(lengths_intron) <- rownames(intron)
intron <- intron[,-1]
head(lengths_intron)

# Remove columns with less than 10 000 reads for introns
countSums_intron <- colSums(intron)
#lowCount_intron = which(countSums_intron < 10000)
# NOTE: Removing the ones with exon count smaller than 10000
intron = intron[,-lowCount]
countSums_intron <- colSums(intron)


# Calculate RPKM values for exons
RPMs <- t(t(exon)/countSums) * 1000000
RPKMs = RPMs / lengths * 1000

# Calculate RPKM values for spikes
RPMs_ecc <- t(t(exon_ecc)/countSums) *1000000
colSums(RPMs_ecc)
RPKMs_ecc <- RPMs_ecc/lengths_ecc * 1000
RPKMs_df_ecc <- data.frame(RPKMs_ecc)

# Calculate RPKM values for introns
RPMs_intron <- t(t(intron)/countSums_intron) * 1000000
RPKMs_intron = RPMs_intron / lengths_intron * 1000

RPKMs_final <- RPKMs - RPKMs_intron
RPKMs_final2 <- RPKMs_intron - RPKMs
rpkm_mean = rowMeans(RPKMs)
rpkm_ecc_mean = rowMeans(RPKMs_ecc)
rpkm_mean[order(-rpkm_mean)]

# make metadata annotation
# bulks 
# Plate 1_H12, Plate1_G12, Plate2_H12, Plate2_G12
nS <- ncol(RPKMs)
colnames(RPKMs)

Q <- read.table("../analysis/delivery/SS2_17_Car_qc.txt",header=T,sep="\t")
# reorder to same as rpkms

setdiff(Q$cell,colnames(RPKMs))
Q <- Q[match(colnames(RPKMs),Q$cell),]

# make metadata annotation
# bulks 
# Plate 1_H12, Plate1_G12, Plate2_H12, Plate2_G12
plate <- unlist(lapply(strsplit(colnames(RPKMs),"_"), function(x) x[1]))
sc.bulk <- rep("sc",nS)
sc.bulk[grep("H12",colnames(RPKMs))]<-"bulk"
sc.bulk[grep("G12",colnames(RPKMs))]<-"bulk"
joint <- paste(plate,sc.bulk,sep="_")

M <- as.data.frame(cbind(plate,sc.bulk,joint))
rownames(M)<-colnames(RPKMs)

# define colors,pch per type
coldef <- c("red","blue","pink","cyan")
names(coldef)<-c("Plate1_sc","Plate2_sc","Plate1_bulk","Plate2_bulk")
col <- coldef[joint]
pch <- rep(16,nS)
pch[M$sc.bulk == "bulk"] <- 17

# color based on detected genes
nDet <- apply(RPKMs,2,detect.genes)
col.det <- color.scale(nDet,c(0,1,1),c(1,1,0),0)

# Check Spike-ins
# calcluate spike-in precentage
frac.spike <- colSums(RPKMs_ecc)/(colSums(RPKMs_ecc)+colSums(RPKMs))
Q["frac.spike"] <- frac.spike
hist(frac.spike,n=100, xlim=c(0.0,0.6), ylim=c(0, 80))

# OBS! some samples with no spikes detected!! Most have very low levels of spike-ins
# check number of detected spikes per sample.
nDet.ercc <- apply(RPKMs_ecc,2,function(x) sum(x>1))
hist(nDet.ercc,n=100)

# 15 spikes with no detection, still average per cell is around 15 detected spikes.
length(which(rowSums(RPKMs_ecc)==0))

plot(colSums(RPKMs_ecc),colSums(RPKMs),col=col,pch=pch)
legend("topright",col=rep(coldef,each=2),pch=c(17,16,17,16),legend=levels(M$joint))

lengthsDF = data.frame(length = lengths, size = "small")
lengthsDF$size = as.character(lengthsDF$size)

lengthsDF$size[lengthsDF$length>1000] = "medium"
lengthsDF$size[lengthsDF$length>5000] = "large"
class(RPMs)
RPMsDF  = data.frame(RPMs)
RPMsDF = cbind(RPMsDF,lengthsDF)
RPMsDF = melt(RPMsDF, id=c("length","size")) 
head(RPMsDF)
RPMsDF$plate = "Plate2"
RPMsDF$plate[grep(pattern = "Plate1", x = RPMsDF$variable)] = "Plate1"

RPMsDF2 = RPMsDF[RPMsDF$value > 1 & RPMsDF$value < 100 ,]
head(RPMsDF2)
RPMsDF2$join = paste(RPMsDF2$plate, RPMsDF2$size, sep ="_")
ggplot(RPMsDF2, aes(y = value,x = as.factor(plate),colour = plate,linetype = size)) + geom_violin(draw_quantiles = c(0.25, 0.5, 0.75)) + facet_grid(size~. )
lenghts2 = lengths[lengths < 5000]
hist(lenghts2)
# Filter Cells
l <- split(1:nS,M$joint)

for (n in colnames(Q)[5:13]){
  print(n)
  d <- lapply(l, function(x) na.omit(Q[x,n]))
  vioplot.list(d,names=names(l),col=rainbow(4))
  mtext(n,outer=T)
}

exon <- as.matrix(exon)
exon_ecc <- as.matrix(exon_ecc)

# clearly 6 cells that deviates, remove those
keep <- which(Q$unique > 60 & Q$frac.spike < 0.2)
samples_keep = intersect(colnames(exon), Q$cell[keep])

M <- M[samples_keep,]
Q <- Q[samples_keep,]
RPKMs <- RPKMs[,samples_keep]
RPKMs_ecc <- RPKMs_ecc[,samples_keep]
exon <- exon_ecc[,samples_keep]
exon_ecc <- exon_ecc[,samples_keep]

col <- col[keep]
pch <- pch[keep]
col.det <- col.det[keep]

nS <- ncol(RPKMs)

# Variable genes based on ERCC
v1 <- cv2.var.genes(RPKMs[,M$joint=="Plate1_sc"],RPKMs_ecc[,M$joint == "Plate1_sc"],plot = T,quant.fit=0.8)
v2 <- cv2.var.genes(R = RPKMs[,M$joint=="Plate2_sc"],ERCC = RPKMs_ecc[,M$joint == "Plate2_sc"],plot = T,quant.fit=0.6)


#PW correlations
Cor <- cor(RPKMs,method="spearman")
h <- hclust(as.dist(1-Cor),method = "ward.D2")
dendro <- as.dendrogram(h)

library(gplots)

h <- heatmap.2(Cor,Colv=dendro,Rowv=dendro,RowSideColors=col.det,ColSideColors=col,scale="none",trace="none",main="Spearman correlation, Ward")


# get max C
maxC <- apply(Cor,1,function(x) max(x[x<1]))

par(mfrow=c(1,2))
hist(maxC[M$plate=="Plate1"],n=100)
hist(maxC[M$plate=="Plate2"],n=100)


# Compare p1 vs p2
# remove all genes expressed in only 1 cell
nDetG <- apply(RPKMs,1,detect.genes)
keepG <- which(nDetG > 1)

LR <- as.matrix(log(RPKMs[keepG,]+1))

# with bulk only
smoothScatter(rowMeans(LR[,M$joint == "Plate1_bulk"]), rowMeans(LR[,M$joint == "Plate2_bulk"]),xlab="Plate1 bulks",ylab="Plate2 bulks")
# with sc data
smoothScatter(rowMeans(LR[,M$joint == "Plate1_sc"]), rowMeans(LR[,M$joint == "Plate2_sc"]),xlab="Plate1 sc",ylab="Plate2 sc")

rm1 <- rowMeans(LR[,M$plate == "Plate1" & M$sc.bulk == "sc"])
rm2 <- rowMeans(LR[,M$plate == "Plate2" & M$sc.bulk == "sc"])
par(mfrow=c(2,1))
hist(rm1,n=100)
hist(rm2,n=100)

sum(rm1==0)
sum(rm2==0)

# Identify genes that clearly differs between P1 & P2
sc1 <- which(M$joint == "Plate1_sc")
sc2 <- which(M$joint == "Plate2_sc")

# remove genes that are not expressed in sc-data
keepG2 <- which(rowSums(RPKMs[,c(sc1,sc2)])>0)

fc <- log2(rowMeans(RPKMs[keepG2,sc1]+1)/rowMeans(RPKMs[keepG2,sc2]+1))
m <- log2(rowMeans(RPKMs[keepG2,c(sc1,sc2)])+1)

# use an arbitrary cutoff of 8 that gives a reasonable number of genes
up1 <- which(fc*m > 8)
up2 <- which(fc*m < -8)
length(up1)
length(up2)

# plot as MA plot
plot(m,fc,pch=1)
points(m[up1],fc[up1],col="red",pch=16)
points(m[up2],fc[up2],col="blue",pch=16)
lengths_Up = lengths[names(m[up1])]
lengths_down = lengths[names(m[up2])]
diffexp = data.frame(lengths = c(lengths_Up,lengths_down), regualtion = as.character("down")) 
diffexp$regualtion = as.character(diffexp$regualtion)
diffexp$regualtion[1:length(lengths_Up)] = "up"

ggplot(diffexp, aes(lengths, color = regualtion)) + geom_density()
# print to file and run Go-term analysis in DAVID
write.table(names(up1),file="qc/plate1_genes_cut8.txt",quote=F,row.names=F,col.names=F)
write.table(names(up2),file="qc/plate2_genes_cut8.txt",quote=F,row.names=F,col.names=F)


#Run pca with each separate dataset and look at top variation

PC1 <- run.pca(RPKMs[keepG2,sc1])
PC2 <- run.pca(RPKMs[keepG2,sc2])
# Comment: We don't get the points in the plot
pca.plot(PC1,col=col.det[sc1],pch=16)
pca.plot(PC2,col=col.det[sc2],pch=16)


de
plot(x = PC1$x[,1],y = PC1$x[,2],col=col.det[sc1])

pca.loadings.plot(PC1,selPC = 1:3)
pca.loadings.plot(PC2,selPC = 1:3)


# compare top loading genes for first 3 pcs - take 20 per direction
nL = 40
pc1.genes <- apply(PC1$rotation[,1:3],2,  function(x) names(sort(x))[c(1:nL, (length(x)-(nL-1)):length(x))])
pc2.genes <- apply(PC2$rotation[,1:3],2,  function(x) names(sort(x))[c(1:nL, (length(x)-(nL-1)):length(x))])

# compare overlap per pc.
overlap_phyper2(as.list(data.frame(pc1.genes)), as.list(data.frame(pc2.genes)),plot=T)

# PC3 for P2 has many genes in common with PC2 for P1. 
# Comment: We get 20, she gets 18
intersect(pc2.genes[,3],pc1.genes[,2])

# also 8 between PC3 for P2 and PC1 for P1
# Comment: We get 5, she gets 8
intersect(pc2.genes[,3],pc1.genes[,1])


# Compare distribution with subcellular localization data
# Comment: Should we use the same file? Changed the subloc$Gene.name to subloc$Gene
subloc <- read.table("../analysis/data/subcellular_location.tsv",header=T,sep="\t")
m<-match(subloc$Gene,exon_gene_names)
genes2 <- exon_gene_names[na.omit(m)]
length(genes2)

subloc <- subloc[match(genes2,subloc$Gene),] #Changed the subloc$Gene.name to subloc$Gene
head(subloc)


# get the unique terms
goterm <- subloc$GO.id
all.terms <- unlist(sapply(as.character(goterm), strsplit, ";"))


# test for significant change in FC for the genes with each of these go-terms
# either using all genes with that annotation, or only genes that has that annotation as a unique entry.

fc <- log2(rowMeans(RPKMs[genes2,sc1]+1)/rowMeans(RPKMs[genes2,sc2]+1))
m <- log2(rowMeans(RPKMs[genes2,c(sc1,sc2)])+1)

library(scales)

plot.goenrich <- function(go.test) {
  #go.test <- "Nucleus (GO:0005634)"
  
  par(mfrow=c(2,2),cex.main=0.7)
  
  # first any that has annotation
  go.genes <- subloc$Gene[grep(go.test,subloc$GO.id,fixed=T)]
  if (length(go.genes) <= 20) {return(0)}
  col.go <- rep("gray",length(genes2))
  col.go[genes2 %in% go.genes]<-"red"
  plot(m,fc,col=alpha(col.go,0.5),main=sprintf("All %s",go.test),pch=16)
  #vioplot - skip genes with m<1
  fc.split <- split(fc[m>1], genes2[m>1] %in% go.genes)
  vioplot.list(fc.split,names = c("not GO","has GO"),col=c("gray","red"))  
  # wilcox test
  w <- wilcox.test(fc.split[[1]],fc.split[[2]],alternative = "two.sided")
  mtext(sprintf("Wilcox pvalue %.2e",w$p.value))
  
  
  # same but only unique
  go.genes <- subloc$Gene[subloc$GO.id == go.test]
  col.go <- rep("gray",length(genes2))
  col.go[genes2 %in% go.genes]<-"red"
  plot(m,fc,col=alpha(col.go,0.5),main=sprintf("Unique %s",go.test),pch=16)
  #vioplot - skip genes with m<1
  fc.split <- split(fc[m>1], genes2[m>1] %in% go.genes)
  vioplot.list(fc.split,names = c("not GO","has GO"),col=c("gray","red"))  
  # wilcox test
  w <- wilcox.test(fc.split[[1]],fc.split[[2]],alternative = "two.sided")
  mtext(sprintf("Wilcox pvalue %.2e",w$p.value))
}

for (gt in unique(all.terms)){
  plot.goenrich(gt)
}


# Same but for gene biotype
# Comment: Might need the following to get biomaRT
# source("https://bioconductor.org/biocLite.R")
# biocLite("biomaRt")
# genes.table = exon_gene_names.table
# gene.table2 = exon_gene_names.table2

suppressMessages(require(biomaRt))
# select which mart to use, in this case ensembl
mart <- useMart("ensembl")
mart <- useDataset("hsapiens_gene_ensembl", mart = mart)

exon_gene_names.table <- getBM(filters= "ensembl_gene_id", attributes= c("ensembl_gene_id", "external_gene_name", "description","gene_biotype"), values= exon_gene_names, mart= mart) 

head(exon_gene_names.table)


# some gene symbols have multiple ensembl ID mappings - reduce into one entry
merge_biotypes <- function(x) {
  x = unique(x)
  if (length(x) == 1) { return(x) }
  else { paste(x,collapse=";")}
}

exon_gene_names.table2 <-aggregate(exon_gene_names.table, by=list(exon_gene_names.table$ensembl_gene_id),merge_biotypes)

########PROBLEMATIC AREA START#####

# some genes has no ensembl translation - remove as well.
genes2 <- exon_gene_names[exon_gene_names %in% exon_gene_names.table2$ensembl_gene_id]

# merge all pseudogene entries into one:
exon_gene_names.table2$gene_biotype[grepl("pseudogene", exon_gene_names.table2$gene_biotype)] <- "pseudogene"

# get only biotypes that applies to at least 20 genes
t <- table(exon_gene_names.table2$gene_biotype)
biotypes <- names(t[t>20])

fc <- log2(rowMeans(RPKMs[genes2,sc1]+1)/rowMeans(RPKMs[genes2,sc2]+1))
m <- log2(rowMeans(RPKMs[genes2,c(sc1,sc2)])+1)

par(mfrow=c(2,2),cex.main=0.7)
for (bt in biotypes){
  # first any that has annotation
  go.genes <- exon_gene_names.table2$external_gene_name[exon_gene_names.table2$gene_biotype == bt]
  col.go <- rep("gray",length(genes2))
  col.go[genes2 %in% go.genes]<-"red"
  plot(m,fc,col=alpha(col.go,0.3),main=bt,pch=16)
  #vioplot - skip genes with m<1
  fc.split <- split(fc[m>1], genes2[m>1] %in% go.genes)
  vioplot.list(fc.split,names = c("not GO","has GO"),col=c("gray","red"))  
  # wilcox test
  w <- wilcox.test(fc.split[[1]],fc.split[[2]],alternative = "two.sided")
  mtext(sprintf("Wilcox pvalue %.2e",w$p.value))  
}

########PROBLEMATIC AREA END#####

# Cell cycle prediction
# convert gene names to ensembl IDs
RPKMs2 <- RPKMs[exon_gene_names.table2$ensembl_gene_id,]
rownames(RPKMs2)<-exon_gene_names.table2$ensembl_gene_id

# Maybe need to do
# source("https://bioconductor.org/biocLite.R")
# biocLite("scran")
# biocLite("scater")

suppressMessages(library(scran))
set.seed(123)
hsa.pairs <- readRDS(system.file("exdata", "human_cycle_markers.rds", package="scran"))

# first for all P1.sc-data
#sce1 <- newSCESet(countData=RPKMs2[,M$joint == "Plate1_sc"])
#class(RPKMs2)
#cc1 <- cyclone(sce1, pairs=hsa.pairs)
cc1 <- cyclone(RPKMs2[,M$joint == "Plate1_sc"], pairs=hsa.pairs)

#sce2 <- newSCESet(countData=RPKMs2[,M$joint == "Plate2_sc"])
#cc2 <- cyclone(sce2, pairs=hsa.pairs)
cc2 <- cyclone(RPKMs2[,M$joint == "Plate2_sc"], pairs=hsa.pairs)

coldef.phase <- c("green","red","blue")
names(coldef.phase) <- c("G1","G2M","S")

pca.plot(PC1,col=coldef.phase[cc1$phases], main="Plate1",selpc=1:3)
pca.plot(PC2,col=coldef.phase[cc2$phases], main="Plate2",selpc=1:3)


# Getting colors for Velocyto
plate1CellState = data.frame(names = paste(colnames(RPKMs2[,M$joint == "Plate1_sc"]), ".Aligned.out", sep=""), col =coldef.phase[cc1$phases], phase = names(coldef.phase[cc1$phases]) )
plate2CellState = data.frame(names = paste(colnames(RPKMs2[,M$joint == "Plate2_sc"]), ".Aligned.out", sep=""), col =coldef.phase[cc2$phases], phase = names(coldef.phase[cc2$phases]) )
write.table(plate1CellState, file = "something.rds", sep = "\t", row.names = FALSE, col.names = TRUE, quote = F)
save(plate1CellState, file = "cellColor.rds")

cell.colors = c(as.character(plate1CellState$col))
names(cell.colors) <- plate1CellState$names
cell.colors = c(as.character(plate2CellState$col))
names(cell.colors) <- plate2CellState$names
save(cell.colors, file="colors.rda")

cell.colors


# PCA with all SC-data

sc <- M$sc.bulk == "sc"
PC.all <- run.pca(RPKMs[,sc])
pca.plot(PC.all, col=col[sc], pch=16, main="PCA with all cells", selpc=1:3)
pca.plot(PC.all, col=col.det[sc], pch=16, main="PCA with all cells", selpc=1:3)
pca.plot(PC.all, col=coldef.phase[c(cc1$phases, cc2$phases)], pch=16, main="PCA with all cells", selpc=1:3)
pca.loadings.plot(PC.all,nPlot = 20, selPC = 1:3)


# Plot Chromosome mapping from samtools idxstats
idxstat <- read.table("../analysis/data/qc/summary_idxstats.csv",sep=",",header=T)
chr.len <-  idxstat$length

# make entries for ERCC, Autosomes, chrX, chrY and chrM
ercc <- grep("ERCC",idxstat$chrom)
chrX <- grep("chrX",idxstat$chrom)
chrY <- grep("chrY",idxstat$chrom)
chrM <- grep("chrM",idxstat$chrom)
rest <- setdiff(1:nrow(idxstat), c(ercc,chrX,chrY,chrM))

mat <- as.matrix(idxstat[, -c(1,2)])

chr.reads <- cbind(colSums(mat[rest,]),mat[chrX,], mat[chrY,],mat[chrM,],colSums(mat[ercc,]))
colnames(chr.reads) <- c("Auto","X","Y","M","ERCC")
barplot(t(chr.reads),col=rainbow(5),las=2,cex.names = 0.5)
legend("topright", fill=rainbow(5), legend = colnames(chr.reads))

# proportion of reads instead
prop <- apply(chr.reads,1,function(x) x/sum(x))
barplot(prop,col=rainbow(5),las=2,cex.names = 0.5)















