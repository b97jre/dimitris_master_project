# functions for extracting neighbours from tsne output as weights into igraph

# set the values for the top nn neighbours as the max distance minus distance
set.nn.value<-function(x,nn,max.val,reverse=FALSE){
        top<-order(x,decreasing=reverse)[2:(nn+1)]
        new<-rep(0,length(x))
	new[top]<-max.val - x[top]
        return(new)
}

# exgtract nn closest neighbours for each point in the tsne output T.
# set weights proportional to the distances in the tsne$Y
select.neighbours.value<-function(T,nn){
	dist<-as.matrix(dist(T$Y))
	maxD<-max(dist)+1
        NN<-apply(dist,2,set.nn.value,nn,maxD)
        return(NN)
}

# exttract nn closest neighbours and give them weights 3,2,1 for top closest neighbours
# e.g. with 9 neighbours, top 3 gets weight 3, top 4-6 gets weight 2 etc.
connect.neighbours<-function(TS,nn,weights=c(3,2,1)){
	dist<-as.matrix(dist(TS$Y))
	nW<-round(nn/length(weights))
	w2<-c()
	for (i in 1:length(weights)){
	    w2<-c(w2,rep(weights[i],nW))
	}
	if (length(w2)>nn){
	   w2<-w2[1:nn]
	}else if (length(w2)<nn) {
	      while(length(w2)<nn) {
	         w2<-c(w2,weights[i])						
	      }
	}

	NN<-apply(dist,2,set.nn.rank,nn,w2)
	return(NN)
}

# functio for setting the correct weights with ranks.
set.nn.rank<-function(x,nn,weigths,reverse=FALSE){
	top<-order(x,decreasing=reverse)[2:(nn+1)]
	new<-rep(0,length(x))
	new[top]<-weights
	return(new)
}
