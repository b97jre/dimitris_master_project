--
title: "ConvertFilesForExternalGeneNames"
author: "Johan"
date: "2/7/2018"
output: html_document
---
  
  
```{r converting the RPKMtable , include= TRUE}
timeStamp = format(Sys.time(), "%Y_%b_%d_%H_%M")


# list of 9 weeks specific parameters
paramsList = list(
  storageDir= "/Users/johanreimegard/storage/dimitris_master_project",
  expressionTable = "countData/RPKMs.table.QC.tsv",
  geneInfoTable =  "information/geneInfo.QC.method.seurat.SCORPIUS.tsv",
  countsTableOut = "countData/RPKMs.table.QC.egn.tsv",
  selection = "external_gene_name")

convertingReportFileName = paste(paramsList$storageDir,
                                   paste(paramsList$countsTableOut, 
                                         "ReduceRows",
                                         paramsList$selection,
                                         timeStamp,"html", sep = "."),
                               sep = "/")


  
rmarkdown::render(input = "ReduceGeneList.Rmd",
                  output_file = convertingReportFileName, 
                  params = paramsList)

```


countData/pulldown.allHours.normalised.seurat.tsv

```{r converting the RPKMtable , include= TRUE}


paramsList = list(
  storageDir= "/Users/johanreimegard/storage/dimitris_master_project",
  expressionTable = "countData/pulldown.allHours.normalised.seurat.tsv",
  geneInfoTable =  "information/geneInfo.QC.method.seurat.SCORPIUS.tsv",
  countsTableOut = "countData/pulldown.allHours.normalised.seurat.egn.tsv",
  selection = "external_gene_name")

convertingReportFileName = paste(paramsList$storageDir,
                                   paste(paramsList$countsTableOut, 
                                         "ReduceRows",
                                         paramsList$selection,
                                         timeStamp,"html", sep = "."),
                               sep = "/")


  
rmarkdown::render(input = "ReduceGeneList.Rmd",
                  output_file = convertingReportFileName, 
                  params = paramsList)

```


```{r converting the RPKMtable , include= TRUE}


paramsList = list(
  storageDir= "/Users/johanreimegard/storage/dimitris_master_project",
  expressionTable = "countData/pulldown.allHours.normalised.seurat.cellCycle.tsv",
  geneInfoTable =  "information/geneInfo.QC.method.seurat.SCORPIUS.tsv",
  countsTableOut = "countData/pulldown.allHours.normalised.seurat.cellCycle.egn.tsv",
  selection = "external_gene_name")

convertingReportFileName = paste(paramsList$storageDir,
                                   paste(paramsList$countsTableOut, 
                                         "ReduceRows",
                                         paramsList$selection,
                                         timeStamp,"html", sep = "."),
                               sep = "/")


  
rmarkdown::render(input = "ReduceGeneList.Rmd",
                  output_file = convertingReportFileName, 
                  params = paramsList)

```