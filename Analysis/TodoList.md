2018
1. Gene loadings for combined protocoll only single cells. 

2. Including Protein step

3.Try the Seurat cell cycle 

4. Pseudo time

5. Include protein data pseudo time 


20180721

1.  Time line analysis 
  using the scorpius data check the protein and mRNA leveles for the genes with both information and plot it.  
  
  
2. For the protein data add the prediction of the cell cycle. 



3. read up on velocity and start running it. 


# 20181016 ANALYSIS 
## QC
1.  Remove the additional samples I identified from the protein data that should be removed (file 0_72h_Protein_exclude_250918.xlsx). Please include a note at the top of the analysis on whether the samples were removed or not.  
2.  For any subsequent analysis, can you filter out proteins with no or less than 3 Cq across all time points in 100 FACS cell control – in other words, if one time point has a Cq value over 3, then include it.  


## Cell cycle
1. In the file protein_comparison_no_norm.Rmd, the cell cycle phase data that is overlayed over the PCA data is from Cyclone. Can this analysis be redone with the data from Seurat’s Cell cycle phase assignments?   
2. Will you try the cell cycle assignment technique as described by Torre bioRxiv 2017?   
3. reCAT 2017   

## RNA-protein co-expression
1. Pseudotime ordering of the RNA and protein values (from file protein_RNA_analysis):  
Normalizing the data in the way so that the RNA and protein data "line-up" better along the Y axis. Is it possibleto do this? In the velocity paper, they multiplied by the degradation rate gamma so that the scales matched. I am not sure if we can do something equivalent.  


## Gene to gene interaction
  Correspond changes in protein expression of pluripotent factors POU5F1, SOX2, NANOG to RNA they target (activate or repress):

* I started to correspond the top genes identified in the pseudotime and the PCA analysis with the genes targeted by POU5F1, SOX2 or NANOG independently or together as identified by CHIP-seq experiments (see Boyer LA Cell 2005). With the gene loadings from the PCA analysis, the genes for PC3 and PC4 (associated with the cell cycle) exclusively match with genes that are targeted by the CHIP experiment control E2F4 (E2F4 regulates the cell cycle). We can probably expand the list of genes we intersect with the chip-seq data. What other data do you suggest we use? Should we extract the RNA data for all the genes identified by the CHIP analysis and see if we see any change over time in these genes?Or how should we try to intersect the data? To discuss.

* Once we have identified genes with evidence of being targeted by the pluripotent factors, can be see any relationships? E.g. in cells with low POU5F1, do we see higher expression of targets that it represses? Or more generally, in cells with lower expression of key pluripotent factors, do we see evidence of greater evidence of differentiation or non-stemness?

## Spliced/ Unspliced data
Can you extract the fraction of unspliced and spliced sequences in a given cell for the following genes and graph themwith the protein data,  ordered by pseudotime??   

| Gene  |
|---|
| METAP1D  |
|LIN28A   |
|POU5F1   |
|LRPAP1   |
|CDH1   |
|FAS   |
|PARK7   |



I realize that might be tricky and messy, but can you the map sequence data for the following genes. The genes should not have polyA tails but are expressed during DNA replication.  They are associated with PC3/cell cycle and they are targets for SOX2/POUF51/NANOG.

| Gene  |
|---|
| HIST1H1A  |
|HIST1H1B   |
|HIST1H1D   |
|HIST1H1E   |
|HIST1H4C   |









## GINI co-efficients
1. Calculate the gini coeffcient for the genes where we have both RNA and protein data.  
2. Calculate the coefficients for the entire data set and for each time point individually.  


## Velocity
We need to know whether it works with this data set or not. So, while not a priority, velocity analysis should be on the list of things to do.


## Manuscript
For the paper methods/results, I need a description of:   

* scRNA dataset processing (e.g. mapping and QC)  
* scRNA QC metrics (e.g. read counts, excluded cells)  
* Steps taken to compare the reference Smart-seq2 and our version of Smart-seq2. Summary of the main differences between the methods.  
* How cell cycle assignment was performed, and a summary of how it differed between the reference Smart-seq2 and our version of Smart-seq2.  

The information I request above does not need to provided in manuscript ready paragraphs. Point form is fine.





  

