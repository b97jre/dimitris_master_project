.PHONY: clean clean-test clean-pyc clean-build docs help conda
.DEFAULT_GOAL := help
define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts


clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

clean-snakemake: ## remove snakemake directories
	find . -name '.snakemake' -exec rm -fr {} +

lint: ## check style with flake8
	flake8 lts_workflows_sm_scrnaseq tests

test: ## run tests quickly with the default Python
	py.test


test-all: ## run tests on every Python version with tox
	tox

coverage: ## check code coverage quickly with the default Python
	coverage run --source lts_workflows_sm_scrnaseq -m pytest
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	rm -f docs/lts_workflows_sm_scrnaseq.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ lts_workflows_sm_scrnaseq lts_workflows_sm_scrnaseq/tests lts_workflows_sm_scrnaseq/rules
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release: clean ## package and upload a release
	python setup.py sdist upload
	python setup.py bdist_wheel upload

CONDA_REPO?=scilifelab-lts
CONDA_OPTIONS?=
current=$(shell git rev-parse --abbrev-ref HEAD)
conda:  ## package and upload a conda release
	git checkout conda
	git merge $(current)
	$(MAKE) clean clean-snakemake
	conda build $(CONDA_OPTIONS) --user $(CONDA_REPO) -m conda/build_config.yaml conda
	git checkout $(current)

DOCKER_REPO?=scilifelablts
DOCKERTAG:=lts-workflows-sm-scrnaseq
DOCKERCITAG:=lts-workflows-sm-scrnaseq-ci
VERSION:=$(shell python setup.py --version)

docker: clean ## Make docker image
	docker build -t ${DOCKER_REPO}/${DOCKERTAG} . --build-arg DOCKER_REPO=$(DOCKER_REPO)

dockerupload: clean docker ## Make docker image and upload
	docker push ${DOCKER_REPO}/${DOCKERTAG}:latest

dockerrelease: clean docker ## Make docker release with version tag and upload
	docker tag ${DOCKER_REPO}/${DOCKERTAG} ${DOCKER_REPO}/${DOCKERTAG}:$(VERSION)
	docker push ${DOCKER_REPO}/${DOCKERTAG}:$(VERSION)

dockerci: clean ## make docker CI image
	docker build -t ${DOCKER_REPO}/${DOCKERCITAG} -f ci/Dockerfile .

dockerciupload: clean dockerci ## make docker ci image and upload
	docker push ${DOCKER_REPO}/${DOCKERCITAG}:latest

dockercirelease: clean docker ## Make docker ci release with version tag and upload
	docker tag ${DOCKER_REPO}/${DOCKERCITAG} ${DOCKER_REPO}/${DOCKERCITAG}:$(VERSION)
	docker push ${DOCKER_REPO}/${DOCKERCITAG}:$(VERSION)

dist: clean ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean ## install the package to the active Python's site-packages
	python setup.py install

bbpipeline: ## Test bitbucket pipeline locally
	@docker run --volume=$(dir $(realpath $(firstword $(MAKEFILE_LIST)))):/repo --workdir="/repo" $(DOCKER_REPO)/$(DOCKERCITAG):latest /bin/bash -c 'ls'
